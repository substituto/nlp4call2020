# Preprocessing pipeline (without text difficulty estimation)

## Python version 3.7.5
For running, do

```
pip install -r requirements.txt
python -m spacy download en_core_web_sm
python main.py
```

For parameters please check main.py. To run the code, add the texts you want to extract sentences from in data->texts.
To run extraction for all instances, run ``` run_extraction.sh ```

