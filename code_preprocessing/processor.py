""" Preprocessing class 
"""
import random
import spacy
from tqdm import tqdm
from collections import defaultdict
import re

class Preprocessing():
    def __init__(self, mwe_list, language='en', maxlen=25):
        """language: is a dummy value right now
           maxlen  : the maximum number of tokens in a sentence"""
        self.nlp = spacy.load("en_core_web_sm")  # Change language here
        # self.nlp.add_pipe(self.nlp.create_pipe('sentencizer'))
        self.mwe_list = [self.nlp(mwe) for mwe in mwe_list]
        self.sentences = []  # List of sentences
        self.random = False
        self.maxlen = 25
        self.result = defaultdict(list)  # mwe -> sentence list

    def add_input_text(self, text):
        """Add a new input text and index sentences """
        # If the number of sentences is not too large, we just use the
        # nltk pre-split sentences to avoid memory issues.
        # alternatively: add chunking into paragraphs here
        if len(text) > 1000000:
            self.sentences = text.split('\n')
        else:
            doc = self.nlp(text)
            # Index sentences
            for sent in [sent.string.strip() for sent in doc.sents]:
                sent = re.sub(r'[\n]+',' ',sent)
                self.sentences.append(sent)

    def process_sentences(self):
        """Filter sentences which contain a specific word sequence"""
        for sentence in tqdm(self.sentences):
            self.check_lemmas(sentence)
        if self.random:
            for sent_list in self.result.values():
                random.shuffle(sent_list)

    def check_lemmas(self, sentence):
        """Filter function for lemmas using spacy """
        nlp_sentence = self.nlp(sentence)
        for mwe in self.mwe_list:
            mwe_lemma = [tok.lemma_ for tok in mwe]
            if len([tok for tok in nlp_sentence]) > self.maxlen:
                continue
            if ' {} '.format(' '.join(mwe_lemma)) not in ' '.join([tok.lemma_ for tok in nlp_sentence]):
                continue
            # Check for substrings:
            if len(' '.join([tok.lemma_ for tok in nlp_sentence]).split(' '.join(mwe_lemma))) > 2:
                continue
            self.result[mwe.text].append(sentence)
