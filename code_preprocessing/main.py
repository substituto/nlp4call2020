import numpy as np
import argparse
from processor import Preprocessing
import os
from collections import defaultdict
import csv

def write_mwe_sentences_to_tsv(mwe_sentences_dict, outupt_file_path):    
    
    sorted_mwe_sent = sorted(
        mwe_sentences_dict.items(), 
        key=lambda kv: kv[0]
    )
    with open(outupt_file_path, 'w') as f_out:
        tsv_writer = csv.writer(f_out, delimiter='\t')
        for mwe, sent_list in sorted_mwe_sent:                    
            for sent in sent_list:                
                tsv_writer.writerow([mwe, sent])


def process_files(mwe_input_file, input_dir, output_dir):

    # read mwe set
    mwe_set = set()
    with open(mwe_input_file) as lines:
        for line in lines:
            mwe_set.add(line.strip())

    input_files = os.listdir(input_dir)
    all_extracted_mwe_sentences = defaultdict(list)

    for file_name in input_files:

        if not file_name.endswith('.txt'):
            continue

        input_file_path = os.path.join(input_dir, file_name)
        print("Reading sentences from: {}".format(input_file_path))

        # Read sentences
        with open(input_file_path) as f_in:
            text = f_in.read()

        # Filter words
        preproc = Preprocessing(mwe_set)
        preproc.add_input_text(text)
        preproc.process_sentences()

        output_file = os.path.join(output_dir, file_name.replace('.txt', '.tsv'))
        write_mwe_sentences_to_tsv(preproc.result, output_file)
        
        for mwe, sent_list in preproc.result.items():
            all_extracted_mwe_sentences[mwe].extend(sent_list)
        
    all_results_file = os.path.join(output_dir, 'all.tsv')
    write_mwe_sentences_to_tsv(all_extracted_mwe_sentences, all_results_file)


"""
Main function handling the arguments
"""
def main():
    parser = argparse.ArgumentParser(description='Preprocessing pipeline')
    parser.add_argument('--input_dir', default=os.path.join('data','texts'),
                        help='Path to the input txt files')
    parser.add_argument('--mwe_input_file', default=os.path.join('data','mwe.txt'),
                        help='Path to the input list of mwe (one per line)')
    parser.add_argument('--output_dir', default=os.path.join('data','results'),
                        help='Path to the output dir')
    args = parser.parse_args()

    mwe_input_file = args.mwe_input_file
    input_dir = args.input_dir
    output_dir = args.output_dir

    process_files(mwe_input_file, input_dir, output_dir)


if __name__ == '__main__':
    main()


