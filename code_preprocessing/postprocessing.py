import argparse
import csv
import string
import os

additional_quotes = ['”','“','‘','’']
all_punct = string.punctuation + ''.join(additional_quotes)
ending_punct = '.!?"'

def fix_quotes(sentence):
    """ Fixes quotation marks:
        * Removes quotation marks if there are too many at the end
        * Adds quotation marks which are missing
    Only used when hard filtering is turned off.
    """
    print("TODO: Implement me!")
    

def hard_filter(sentence):
    cleaned_sentence = sentence.strip().replace('”','"').replace('“','"')

    if any(map(cleaned_sentence.startswith, all_punct)):
        return "None"
    elif cleaned_sentence.count('"')%2 != 0:
        return "None"
    elif cleaned_sentence[0].islower():
        return "None"
    elif not any(map(cleaned_sentence.endswith, ending_punct)):
        return "None"
    else:
        return sentence 


"""
Main function handling the arguments
"""
def main():
    parser = argparse.ArgumentParser(description='Preprocessing pipeline')
    parser.add_argument('--input', default=os.path.join('data','results','all.tsv'),
                        help='Path to the input txt files')
    parser.add_argument('--output', default=os.path.join('data','results','filtered_all.tsv'),
                        help='Path to the output txt files')
    parser.add_argument('--hard-filter', type=bool, default=True,
                        help='Filter everything potentially wrong.')
    args = parser.parse_args()

    infile = args.input
    outfile = args.output
    filtering = args.hard_filter

    with open(infile,'r') as inlines, open(outfile,'w') as outlog:
        reader = csv.reader(inlines, delimiter='\t', quotechar='"') 
        writer = csv.writer(outlog, delimiter='\t')
        for line in reader:
            if hard_filter(line[1]) is not "None":
                writer.writerow(line)

if __name__ == '__main__':
    main()

