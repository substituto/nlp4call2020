This repository contains the code related to the publication:

_Marianne Grace Araneta, Gülşen Eryiğit, Alexander König, Ji-Ung Lee, Ana Luís, Verena Lyding, Lionel Nicolas, Christos Rodosthenous, Federico Sangati_<br>
**Substituto - A Synchronous Educational Language Game for Simultaneous Teaching and Crowdsourcing.**

A working version of the bot can be tried in Telegram at the following url: 
https://t.me/ReWordGameBot
