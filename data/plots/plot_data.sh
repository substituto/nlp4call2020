#!/usr/bin/gnuplot -persist
set term pdfcairo font "Arial,16"
set output "crowdsourcing-plot.pdf"
set datafile separator "\t"

file1="plot_data.csv"

set key Left
set key bottom right

set grid
set ytics 0.1
set format y ""
set format x ""
set multiplot layout 1,1 rowsfirst
set style fill transparent solid 0.3 noborder

set arrow from 1.,0.51 to 6.,0.51 nohead front lc "#0d74db" lw 4  dt 2 
set arrow from 1.,0.66 to 6.,0.66 nohead front lc "#dec20d" lw 4  dt 2 
set arrow from 1.,0.63 to 6.,0.63 nohead front lc "#d90d39" lw 4  dt 2 

set yrange[0:1]
plot file1 using 1:2 w lp title "        " lw 4 lc "#0d74db" pt 9 ps  1.2, \
     '' using 1:3 w lp title "        " lw 4 lc "#dec20d" pt 11 ps  1.2, \
     '' using 1:4 w lp title "        " lw 4 lc "#d90d39" pt 13 ps  1.2



unset multiplot
unset output
unset term


