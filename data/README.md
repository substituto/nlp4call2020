# Data description

data.csv contains an anonymized version of the data we collected in our pilot study. All participants provided their consent to publish an anonymized verison of the data.

1. answer -> a student's answer
1. votes -> how many votes this answer got (contains the same number for the same answers. Needs to be aggregated first into a set of answers if you want to compute the number of total votes per round)
1. correct -> indicates if the teacher deemed the answer correct (1) or not (0)

the folder plots contains aggregated data for our data analysis plot.